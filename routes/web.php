<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/firm', [App\Http\Controllers\FarmController::class, 'index']);
Route::get('/firm/add', [App\Http\Controllers\FarmController::class, 'create']);
Route::post('firm/store', [App\Http\Controllers\FarmController::class, 'store'])->name('storeFirm');
Route::get('/firm/edit/{id}', [App\Http\Controllers\FarmController::class, 'edit']);
Route::post('firm/update/{id}', [App\Http\Controllers\FarmController::class, 'update'])->name('updateFirm');
Route::get('firm/delete/{id}', [App\Http\Controllers\FarmController::class, 'delete'])->name('deleteFirm');

Route::get('/supplier', [App\Http\Controllers\SupplierController::class, 'index']);
Route::get('/supplier/add', [App\Http\Controllers\SupplierController::class, 'create']);
Route::post('supplier/store', [App\Http\Controllers\SupplierController::class, 'store'])->name('storeSupplier');
Route::get('/supplier/edit/{id}', [App\Http\Controllers\SupplierController::class, 'edit']);
Route::post('supplier/update/{id}', [App\Http\Controllers\SupplierController::class, 'update'])->name('updateSupplier');
Route::get('supplier/delete/{id}', [App\Http\Controllers\SupplierController::class, 'delete'])->name('deleteSupplier');

Route::get('/invoice', [App\Http\Controllers\InvoiceController::class, 'index']);
Route::get('/invoice/add', [App\Http\Controllers\InvoiceController::class, 'create']);
Route::post('invoice/store', [App\Http\Controllers\InvoiceController::class, 'store'])->name('storeInvoice');
Route::get('/invoice/edit/{id}', [App\Http\Controllers\InvoiceController::class, 'edit']);
Route::post('invoice/update/{id}', [App\Http\Controllers\InvoiceController::class, 'update'])->name('updateInvoice');
Route::get('invoice/delete/{id}', [App\Http\Controllers\InvoiceController::class, 'delete'])->name('deleteInvoice');

Route::get('/paid_invoices', [App\Http\Controllers\PaidInvoicesController::class, 'index']);
Route::get('/paid_invoices/add', [App\Http\Controllers\PaidInvoicesController::class, 'create']);
Route::post('paid_invoices/store', [App\Http\Controllers\PaidInvoicesController::class, 'store'])->name('storepaid_invoices');
Route::get('/paid_invoices/edit/{id}', [App\Http\Controllers\PaidInvoicesController::class, 'edit']);
Route::post('paid_invoices/update/{id}', [App\Http\Controllers\PaidInvoicesController::class, 'update'])->name('updatepaid_invoices');
Route::get('paid_invoices/delete/{id}', [App\Http\Controllers\PaidInvoicesController::class, 'delete'])->name('deletepaid_invoices');

Route::get('/supplier_report', [App\Http\Controllers\SupplierReportController::class, 'index']);

