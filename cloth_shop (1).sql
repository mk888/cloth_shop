-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 08, 2021 at 11:23 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cloth_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `firms`
--

CREATE TABLE `firms` (
  `firm_id` int(4) NOT NULL,
  `farm_name` varchar(200) NOT NULL,
  `farm_owner` varchar(100) NOT NULL,
  `gst_no` varchar(30) DEFAULT NULL,
  `pan_no` varchar(30) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified_by` int(4) DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `firms`
--

INSERT INTO `firms` (`firm_id`, `farm_name`, `farm_owner`, `gst_no`, `pan_no`, `address`, `is_active`, `created_by`, `created_at`, `modified_by`, `modified_at`) VALUES
(1, 'mahalaxmi saree center', 'Bhalchandra Kapadane', 'IUSIU876876JH', 'EDEPK6760R', 'Kalwan', 1, 1, '2021-08-29 16:30:31', NULL, NULL),
(2, 'Navnath Vasralay', 'Kamlesh kapadane', 'JSYS9898hHHh', 'UJYHI9876I', 'Kalwan,Nashik', 1, 1, '2021-08-29 16:38:19', NULL, NULL),
(3, 'mahalaxmi collections', 'Suraj Kapadane', 'UJHYTRTYIKK', 'IJUHY7878Y', 'Kalwan,dist-nashik', 1, 1, '2021-08-29 16:40:22', NULL, NULL),
(4, 'mahalaxmi saree center 1', 'Bhalchandra Kapadane', 'IUSIU876876JH', 'UJYHI9876I', 'Kalwan,Nashik', 0, 1, '2021-08-30 14:51:09', NULL, NULL),
(5, 'Navnath Vasralays', 'Kamlesh kapadane', 'IUSIU876876JH', 'IJUHY7878Y', 'Kalwan,dist-nashik', 0, 1, '2021-08-30 14:52:53', NULL, NULL),
(6, ',mans,m', 'mn,ns,', 'm,ns,dm', ',md', ',mn,m', 0, 1, '2021-08-30 14:59:12', NULL, NULL),
(7, 'Shantusha Collection', 'Yugesh kapadane', NULL, 'EIIIK6760R', NULL, 1, 1, '2021-09-08 07:06:34', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `invoice_id` int(4) NOT NULL,
  `farm_id` int(4) NOT NULL,
  `supplier_id` int(4) NOT NULL,
  `invoice_no` varchar(30) NOT NULL,
  `invoice_date` date NOT NULL,
  `parcel_no` varchar(20) DEFAULT NULL,
  `parcel_through` varchar(20) DEFAULT NULL,
  `total_parcel` int(4) DEFAULT NULL,
  `amount` decimal(15,2) NOT NULL DEFAULT 0.00,
  `sgst` decimal(15,2) DEFAULT 0.00,
  `cgst` decimal(15,2) DEFAULT 0.00,
  `igst` decimal(15,2) DEFAULT 0.00,
  `total_amount` decimal(15,2) NOT NULL DEFAULT 0.00,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified_by` int(4) DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`invoice_id`, `farm_id`, `supplier_id`, `invoice_no`, `invoice_date`, `parcel_no`, `parcel_through`, `total_parcel`, `amount`, `sgst`, `cgst`, `igst`, `total_amount`, `is_active`, `created_by`, `created_at`, `modified_by`, `modified_at`) VALUES
(1, 1, 1, '101', '2021-08-18', '112233', 'BITCO', 3, '90000.00', '0.00', '0.00', '500.00', '90500.00', 1, 1, '2021-08-31 09:25:12', NULL, NULL),
(2, 1, 2, '102', '2021-09-01', '767877', 'BITCO', 2, '125000.00', '0.00', '0.00', '6250.00', '131250.00', 1, 1, '2021-08-31 09:38:36', NULL, NULL),
(3, 1, 1, 'ABC565', '2021-09-01', '112266', 'BITCO', 3, '110000.00', '0.00', '0.00', '500.00', '110500.00', 1, 1, '2021-09-01 15:12:20', NULL, NULL),
(4, 1, 3, 'INV-301', '2021-09-08', NULL, NULL, NULL, '51000.00', '0.00', '0.00', '1000.00', '52000.00', 1, 1, '2021-09-08 07:35:46', NULL, NULL),
(5, 1, 3, 'INV-1001', '2021-09-08', NULL, NULL, NULL, '21000.00', '0.00', '0.00', '1100.00', '22100.00', 1, 1, '2021-09-08 08:18:37', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `paid_invoices`
--

CREATE TABLE `paid_invoices` (
  `paid_invoice_id` int(4) NOT NULL,
  `firm_id` int(4) NOT NULL,
  `supplier_id` int(4) NOT NULL,
  `paid_amount` decimal(15,2) NOT NULL,
  `paid_date` date NOT NULL,
  `paid_by` varchar(30) NOT NULL,
  `transaction_no` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `paid_invoices`
--

INSERT INTO `paid_invoices` (`paid_invoice_id`, `firm_id`, `supplier_id`, `paid_amount`, `paid_date`, `paid_by`, `transaction_no`, `is_active`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 1, '90000.00', '2021-09-01', 'RTGS', '101ABC', 1, 1, '2021-09-01 10:02:14', NULL, NULL),
(2, 1, 2, '100000.00', '2021-09-01', 'CASH', '101', 1, 1, '2021-09-01 14:49:44', NULL, NULL),
(3, 1, 1, '61000.00', '2021-09-08', 'RTGS', 'BHUYTH', 1, 1, '2021-09-08 07:46:03', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `supplier_id` int(4) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `contact_no` bigint(11) NOT NULL,
  `email_id` varchar(50) DEFAULT NULL,
  `state` varchar(30) NOT NULL,
  `state_code` varchar(30) DEFAULT NULL,
  `city` varchar(30) NOT NULL,
  `address` varchar(255) NOT NULL,
  `gst_no` varchar(30) DEFAULT NULL,
  `pan_no` varchar(30) DEFAULT NULL,
  `account_no` varchar(30) DEFAULT NULL,
  `bank_name` varchar(30) DEFAULT NULL,
  `ifsc_code` varchar(30) DEFAULT NULL,
  `branch_code` varchar(30) DEFAULT NULL,
  `upi` varchar(30) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(4) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`supplier_id`, `supplier_name`, `contact_no`, `email_id`, `state`, `state_code`, `city`, `address`, `gst_no`, `pan_no`, `account_no`, `bank_name`, `ifsc_code`, `branch_code`, `upi`, `is_active`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'Hafiz Traders', 999999999, 'hafiz@gmial.com', 'Utar Pradesh', '026', 'Mahuu', 'Mahuu', 'HYJUI89078JUHY', 'JHYUI8767U', '320052859558', 'SBI', 'SBIN00000115', 'SBIN015', 'hafiz@ybl', 1, 1, '2021-08-30 16:54:54', NULL, NULL),
(2, 'Manhorlal Damodar Das', 9898989898, 'damodas@gmail.com', 'Maharashtra', '016', 'Dhule', 'lane No 6, Shop No - 211', 'UJHYGTRFT7987', 'JHGJ7HJG8', '325698526385', 'HDFC Bank', 'HDFCI5001', '016', 'damodas@bbl', 1, 1, '2021-08-30 17:15:26', NULL, NULL),
(3, 'durga texttills', 9898989797, 'kapadane.mahesh@gmail.com', 'Maharashtra', NULL, 'surat', 'ring road', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2021-09-08 07:34:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'mk', 'mk@gmail.com', NULL, '$2y$10$u0qUip84GWiFQKgYDGvFou8dnh8eNssCmrKGW0oylQXBXws2tHnB6', NULL, '2021-08-26 01:17:56', '2021-08-26 01:17:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `firms`
--
ALTER TABLE `firms`
  ADD PRIMARY KEY (`firm_id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paid_invoices`
--
ALTER TABLE `paid_invoices`
  ADD PRIMARY KEY (`paid_invoice_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `firms`
--
ALTER TABLE `firms`
  MODIFY `firm_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `invoice_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `paid_invoices`
--
ALTER TABLE `paid_invoices`
  MODIFY `paid_invoice_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `supplier_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
