@extends('layouts.app')
@section('content')
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Supplier</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{ route('storeSupplier') }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-6">
                      <label for="exampleInputEmail1">Supplier Name</label>
                      <input type="text" class="form-control" name ="supplier_name" id="supplier_name" placeholder="Enter Supplier Name" required>
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">Contact No.</label>
                      <input type="text" class="form-control" name ="contact_no" id="contact_no" placeholder="Enter Contact No." required>
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">Email No.</label>
                      <input type="text" class="form-control" name ="email_id" id="email_id" placeholder="Enter Email Id">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">GST IN No.</label>
                      <input type="text" class="form-control" name ="gst_no" id="gst_no" placeholder="Enter GST IN No.">
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">PAN No.</label>
                      <input type="text" class="form-control" name ="pan_no" id="pan_no" placeholder="Enter PAN No.">
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">State</label>
                      <input type="text" class="form-control" name ="state" id="state" placeholder="Enter State" required>
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">State Code</label>
                      <input type="text" class="form-control" name ="state_code" id="state_code" placeholder="Enter State Code">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-4">
                      <label for="exampleInputEmail1">City</label>
                      <input type="text" class="form-control" name ="city" id="city" placeholder="Enter City" required>
                    </div>
                    <div class="form-group col-8">
                      <label for="exampleInputEmail1">Address</label>
                      <input type="text" class="form-control" name ="address" id="address" placeholder="Enter Address" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">Bank Name</label>
                      <input type="text" class="form-control" name ="bank_name" id="bank_name" placeholder="Enter Bank Name">
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">Account No.</label>
                      <input type="text" class="form-control" name ="account_no" id="account_no" placeholder="Enter Account No">
                    </div>
                    <div class="form-group col-2">
                      <label for="exampleInputEmail1">Branch Code</label>
                      <input type="text" class="form-control" name ="branch_code" id="branch_code" placeholder="Enter Branch Code">
                    </div>
                    <div class="form-group col-2">
                      <label for="exampleInputEmail1">IFSC CODE</label>
                      <input type="text" class="form-control" name ="ifsc_code" id="ifsc_code" placeholder="Enter IFSC Code">
                    </div>
                    <div class="form-group col-2">
                      <label for="exampleInputEmail1">UPI</label>
                      <input type="text" class="form-control" name ="upi" id="upi" placeholder="Enter UPI">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->


        
     
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  @endsection
@include('layouts.script')