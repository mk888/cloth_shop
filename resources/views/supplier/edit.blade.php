@extends('layouts.app')
@section('content')
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Supplier</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{ route('updateSupplier',[$SupplierAll[0]->supplier_id]) }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-6">
                      <label for="exampleInputEmail1">Supplier Name</label>
                      <input type="text" class="form-control" name ="supplier_name" id="supplier_name" placeholder="Enter Supplier Name" value="{{$SupplierAll[0]->supplier_name}}" required>
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">Contact No.</label>
                      <input type="text" class="form-control" name ="contact_no" id="contact_no" placeholder="Enter Contact No." value="{{$SupplierAll[0]->contact_no}}" required>
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">Email No.</label>
                      <input type="text" class="form-control" name ="email_id" id="email_id" placeholder="Enter Email Id" value="{{$SupplierAll[0]->email_id}}">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">GST IN No.</label>
                      <input type="text" class="form-control" name ="gst_no" id="gst_no" placeholder="Enter GST IN No." value="{{$SupplierAll[0]->gst_no}}">
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">PAN No.</label>
                      <input type="text" class="form-control" name ="pan_no" id="pan_no" placeholder="Enter PAN No." value="{{$SupplierAll[0]->pan_no}}">
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">State</label>
                      <input type="text" class="form-control" name ="state" id="state" placeholder="Enter State" value="{{$SupplierAll[0]->state}}" required>
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">State Code</label>
                      <input type="text" class="form-control" name ="state_code" id="state_code" placeholder="Enter State Code" value="{{$SupplierAll[0]->state_code}}">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-4">
                      <label for="exampleInputEmail1">City</label>
                      <input type="text" class="form-control" name ="city" id="city" placeholder="Enter City" value="{{$SupplierAll[0]->city}}" required>
                    </div>
                    <div class="form-group col-8">
                      <label for="exampleInputEmail1">Address</label>
                      <input type="text" class="form-control" name ="address" id="address" placeholder="Enter Address" value="{{$SupplierAll[0]->address}}" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">Bank Name</label>
                      <input type="text" class="form-control" name ="bank_name" id="bank_name" placeholder="Enter Bank Name" value="{{$SupplierAll[0]->bank_name}}">
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">Account No.</label>
                      <input type="text" class="form-control" name ="account_no" id="account_no" placeholder="Enter Account No" value="{{$SupplierAll[0]->account_no}}">
                    </div>
                    <div class="form-group col-2">
                      <label for="exampleInputEmail1">Branch Code</label>
                      <input type="text" class="form-control" name ="branch_code" id="branch_code" placeholder="Enter Branch Code" value="{{$SupplierAll[0]->branch_code}}">
                    </div>
                    <div class="form-group col-2">
                      <label for="exampleInputEmail1">IFSC CODE</label>
                      <input type="text" class="form-control" name ="ifsc_code" id="ifsc_code" placeholder="Enter IFSC Code" value="{{$SupplierAll[0]->ifsc_code}}">
                    </div>
                    <div class="form-group col-2">
                      <label for="exampleInputEmail1">UPI</label>
                      <input type="text" class="form-control" name ="upi" id="upi" placeholder="Enter UPI" value="{{$SupplierAll[0]->upi}}">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->


        
     
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  @endsection
@include('layouts.script')