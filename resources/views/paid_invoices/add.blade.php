@extends('layouts.app')
@section('content')
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Paid Invoice</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{ route('storepaid_invoices') }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                   <div class="form-group col-4">
                      <label>Select Firm</label>
                      <select class="form-control" name="firm_id" id="firm_id" required>
                        <option value="">Select Firm</option>
                        @foreach($FarmAll as $firm)
                          <option value="{{$firm->firm_id}}">{{$firm->farm_name}}</option>
                        @endforeach              
                      </select>
                    </div>
                    <div class="form-group col-4">
                      <label>Select Supplier</label>
                      <select class="form-control" name="supplier_id" id="supplier_id" required>
                        <option value="">Select Supplier</option>
                        @foreach($SupplierAll as $Supplier)
                          <option value="{{$Supplier->supplier_id}}">{{$Supplier->supplier_name}}</option>
                        @endforeach              
                      </select>
                    </div>
                    <div class="form-group col-4">
                      <label for="exampleInputEmail1">Paid Amount</label>
                      <input type="number" class="form-control" name ="paid_amount" id="paid_amount" placeholder="Enter Paid Amount" required>
                    </div>                    
                  </div>
                  <div class="row">
                    <div class="form-group col-4">
                      <label>Paid Date</label>
                        <div class="input-group date" id="reservationdate" data-target-input="nearest">
                            <input type="text" name="paid_date" class="form-control datetimepicker-input" data-target="#reservationdate"/ required>
                            <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-4">
                      <label>Paid By</label>
                      <select class="form-control" name="paid_by" id="paid_by" required>
                        <option value="">Select Paid By</option>
                        <option value="CASH">CASH</option>
                        <option value="RTGS">RTGS</option>
                        <option value="NEFT">NEFT</option>
                        <option value="IMPS">IMPS</option>
                        <option value="UPI">UPI</option>                                   
                      </select>
                    </div>
                    <div class="form-group col-4">
                      <label for="exampleInputEmail1">Transaction No.</label>
                      <input type="text" class="form-control" name ="transaction_no" id="transaction_no" placeholder="Enter Parcel Through">
                    </div>
                   
                  </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->


        
     
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  @endsection
@include('layouts.script')
<script type="text/javascript">
  $(function () {
    $('#reservationdate').datetimepicker({
        format: 'YYYY/MM/DD'
    });
  })
</script>