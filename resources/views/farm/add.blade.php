@extends('layouts.app')
@section('content')
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Firm</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{ route('storeFirm') }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-6">
                      <label for="exampleInputEmail1">Firm Name</label>
                      <input type="text" class="form-control" name ="farm_name" id="farm_name" placeholder="Enter Firm Name" required>
                    </div>
                    <div class="form-group col-6">
                      <label for="exampleInputEmail1">Firm Owner</label>
                      <input type="text" class="form-control" name ="farm_owner" id="farm_owner" placeholder="Enter Firm Owner" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">GST IN No.</label>
                      <input type="text" class="form-control" name ="gst_no" id="gst_no" placeholder="Enter GST IN No.">
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">PAN No.</label>
                      <input type="text" class="form-control" name ="pan_no" id="pan_no" placeholder="Enter PAN No.">
                    </div>
                    <div class="form-group col-6">
                      <label for="exampleInputEmail1">Address</label>
                      <input type="text" class="form-control" name ="address" id="address" placeholder="Enter Address">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->


        
     
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  @endsection
@include('layouts.script')