@extends('layouts.app')
@section('content')
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Invoice</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{ route('updateInvoice',[$InvoiceData[0]->invoice_id]) }}">
                @csrf
                <div class="card-body">
                  <div class="row">
                   <div class="form-group col-3">
                      <label>Select Firm</label>
                      <select class="form-control" name="farm_id" id="farm_id" required>
                        <option value="">Select Firm</option>
                        @foreach($FarmAll as $firm)
                          @if ($InvoiceData[0]->farm_id == $firm->firm_id)                          
                          <option value="{{$firm->firm_id}}" selected>{{$firm->farm_name}}</option>
                          @else                          
                          <option value="{{$firm->firm_id}}">{{$firm->farm_name}}</option>
                          @endif
                        @endforeach              
                      </select>
                    </div>
                    <div class="form-group col-3">
                      <label>Select Supplier</label>
                      <select class="form-control" name="supplier_id" id="supplier_id" required>
                        <option value="">Select Supplier</option>
                        @foreach($SupplierAll as $Supplier)
                          @if ($InvoiceData[0]->supplier_id == $Supplier->supplier_id)
                          <option value="{{$Supplier->supplier_id}}" selected>{{$Supplier->supplier_name}}</option>
                          @else 
                          <option value="{{$Supplier->supplier_id}}">{{$Supplier->supplier_name}}</option>
                          @endif
                        @endforeach              
                      </select>
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">Invoice No</label>
                      <input type="text" class="form-control" name ="invoice_no" id="invoice_no" placeholder="Enter Invoice No." value="{{$InvoiceData[0]->invoice_no}}" required>
                    </div>
                    <div class="form-group col-3">
                      <label>Invoice Date</label>
                        <div class="input-group date" id="reservationdate" data-target-input="nearest">
                            <input type="text" name="invoice_date" value="{{$InvoiceData[0]->invoice_date}}" class="form-control datetimepicker-input" data-target="#reservationdate"/ required>
                            <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">Parcel No</label>
                      <input type="text" class="form-control" name ="parcel_no" id="parcel_no" value="{{$InvoiceData[0]->parcel_no}}" placeholder="Enter Parcel No.">
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">Parcel Through</label>
                      <input type="text" class="form-control" name ="parcel_through" id="parcel_through" value="{{$InvoiceData[0]->parcel_through}}" placeholder="Enter Parcel Through">
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">Total Parcel</label>
                      <input type="text" class="form-control" name ="total_parcel" id="total_parcel" value="{{$InvoiceData[0]->total_parcel}}" placeholder="Enter Total Parcel">
                    </div>
                     <div class="form-group col-3">
                      <label for="exampleInputEmail1">Amount</label>
                      <input type="number" class="form-control amt" name ="amount" id="amount" value="{{$InvoiceData[0]->amount}}" placeholder="Enter Amount" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">SGST</label>
                      <input type="number" class="form-control amt" name ="sgst" id="sgst" value="{{$InvoiceData[0]->sgst}}" placeholder="Enter SGST">
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">CGST</label>
                      <input type="number" class="form-control amt" name ="cgst" id="cgst" value="{{$InvoiceData[0]->cgst}}" placeholder="Enter CGST">
                    </div>
                    <div class="form-group col-3">
                      <label for="exampleInputEmail1">IGST</label>
                      <input type="number" class="form-control amt" name ="igst" id="igst" value="{{$InvoiceData[0]->igst}}" placeholder="Enter IGST">
                    </div>
                     <div class="form-group col-3">
                      <label for="exampleInputEmail1">Total Amount</label>
                      <input type="number" class="form-control" name ="total_amount" id="total_amount" placeholder="Enter Total Amount" value="{{$InvoiceData[0]->total_amount}}" required>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->


        
     
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  @endsection
@include('layouts.script')
<script type="text/javascript">
  $(function () {
    $('#reservationdate').datetimepicker({
        format: 'YYYY/MM/DD'
    });
  });
  $(document).ready(function(){
    $(".amt").on("keyup", function(){
      var value1 = document.getElementById('amount').value;
      var value2 = document.getElementById('sgst').value;
      var value3 = document.getElementById('cgst').value;
      var value4 = document.getElementById('igst').value;
      var sum = parseInt(value1) + parseInt(value2) + parseInt(value3) + parseInt(value4);
      $("#total_amount").val(sum);
    });
  });
</script>