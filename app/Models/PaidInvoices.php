<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class PaidInvoices extends Model
{
	protected $table = 'paid_invoices';
}