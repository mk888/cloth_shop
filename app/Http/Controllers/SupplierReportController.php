<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\PaidInvoices;

class SupplierReportController extends Controller
{
    public function index()
    {
       $invoices = DB::select('SELECT SUM(`total_amount`) as total_amout,
         suppliers.supplier_id,
         suppliers.supplier_name,
         firms.firm_id,
         firms.farm_name
         FROM 
         invoices INNER JOIN suppliers ON
         invoices.supplier_id = suppliers.supplier_id
         INNER JOIN firms ON invoices.farm_id = firms.firm_id
         GROUP by invoices.supplier_id,invoices.farm_id,suppliers.supplier_id,suppliers.supplier_name,firms.firm_id,firms.farm_name');
         $paid_invoices = DB::select('SELECT SUM(`paid_amount`) as paid_amount,supplier_id,firm_id FROM `paid_invoices` 
         GROUP BY `firm_id`,`supplier_id`');
         $reports = [];
         foreach($invoices as $key => $value)
         {
            $reports[$key]['supplier_name']  = $value->supplier_name;
            $reports[$key]['firm_name']      = $value->farm_name;
            $reports[$key]['total_amount']   = $value->total_amout;
            foreach($paid_invoices as $key1 => $value1)
            {
               if($value1->firm_id == $value->firm_id && $value1->supplier_id  == $value->supplier_id)
               {
                  $reports[$key]['paid_amount']   = $value1->paid_amount;
                  $reports[$key]['outstanding']   = $value->total_amout - $value1->paid_amount.'.00';

               }
            }
         }
         foreach($reports as $key => $value)
         {
            if(!isset($value['paid_amount']))
            {
               $reports[$key]['paid_amount'] = 0.00;
               $reports[$key]['outstanding'] = $value['total_amount'];
            }
         }

        return view('supplier_report.supplier_report',compact('reports'));
    }
    
}
