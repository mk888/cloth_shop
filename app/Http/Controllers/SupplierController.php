<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Suppliers;

class SupplierController extends Controller
{
    public function index()
    {
       $SupplierAll = DB::table('suppliers')
       ->select('*')
       ->where('is_active',1)
       ->get();
        return view('supplier.supplier',compact('SupplierAll'));
    }
    public function create()
    {
        return view('supplier.add');
    }
    public function store(Request $request)
    {
       Suppliers::insert([
        'supplier_name'     => $request->supplier_name,
        'contact_no'        => $request->contact_no,
        'email_id'          => $request->email_id,
        'state'             => $request->state,
        'state_code'        => $request->state_code,
        'city'              => $request->city,
        'address'           => $request->address,
        'gst_no'            => $request->gst_no,
        'pan_no'            => $request->pan_no,
        'account_no'        => $request->account_no,
        'bank_name'         => $request->bank_name,
        'ifsc_code'         => $request->ifsc_code,
        'branch_code'       => $request->branch_code,
        'upi'               => $request->upi,
        'created_by'        => auth()->user()->id
       ]);
       return redirect('supplier')->with('success','Supplier Added Successfully!!!');
    }
    public function edit($id)
    {
        $SupplierAll = DB::select('select * from suppliers where supplier_id="'.$id.'"');
        return view('supplier.edit',compact('SupplierAll'));
    }
    public function update(Request $request,$id)
    {
       DB::table('suppliers')
       ->where('supplier_id',$id)
       ->update([
        'supplier_name'     => $request->supplier_name,
        'contact_no'        => $request->contact_no,
        'email_id'          => $request->email_id,
        'state'             => $request->state,
        'state_code'        => $request->state_code,
        'city'              => $request->city,
        'address'           => $request->address,
        'gst_no'            => $request->gst_no,
        'pan_no'            => $request->pan_no,
        'account_no'        => $request->account_no,
        'bank_name'         => $request->bank_name,
        'ifsc_code'         => $request->ifsc_code,
        'branch_code'       => $request->branch_code,
        'upi'               => $request->upi,
       ]);
       return redirect('supplier')->with('success','Supplier Updated Successfully!!!');
    }
    public function delete($id)
    {
       DB::table('suppliers')
       ->where('supplier_id',$id)
       ->update([
        'is_active'     => 0
       ]);
       return redirect('supplier')->with('success','Supplier Deleted Successfully!!!');
    }
}
