<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Invoices;

class InvoiceController extends Controller
{
    public function index()
    {
       $InvoiceAll = DB::table('invoices')
       ->select('invoices.*','firms.farm_name','suppliers.supplier_name')
       ->join('firms','invoices.farm_id','=','firms.firm_id')
       ->join('suppliers','invoices.supplier_id','=','suppliers.supplier_id')
       ->where('invoices.is_active',1)
       ->get();
        return view('invoice.invoice',compact('InvoiceAll'));
    }
    public function create()
    {
      $FarmAll = DB::table('firms')->select('firm_id','farm_name')->where('is_active',1)->get();
      $SupplierAll = DB::table('suppliers')->select('supplier_id','supplier_name')->where('is_active',1)->get();
      return view('invoice.add',compact('FarmAll','SupplierAll'));

    }
    public function store(Request $request)
    {
       Invoices::insert([
        'farm_id'          => $request->farm_id,
        'supplier_id'      => $request->supplier_id,
        'invoice_no'       => $request->invoice_no,
        'invoice_date'     => $request->invoice_date,
        'parcel_no'        => $request->parcel_no,
        'parcel_through'   => $request->parcel_through,
        'total_parcel'     => $request->total_parcel,
        'amount'           => $request->amount,
        'sgst'             => $request->sgst,
        'cgst'             => $request->cgst,
        'igst'             => $request->igst,
        'total_amount'     => $request->total_amount,
        'created_by'       => auth()->user()->id
       ]);
       return redirect('invoice')->with('success','Invoice Added Successfully!!!');
    }
    public function edit($id)
    {
        $FarmAll = DB::table('firms')->select('firm_id','farm_name')->where('is_active',1)->get();
        $SupplierAll = DB::table('suppliers')->select('supplier_id','supplier_name')->where('is_active',1)->get();
        $InvoiceData = DB::select('select * from invoices where invoice_id="'.$id.'"');
        return view('invoice.edit',compact('InvoiceData','FarmAll','SupplierAll'));
    }
    public function update(Request $request,$id)
    {
       DB::table('invoices')
       ->where('invoice_id',$id)
       ->update([
        'farm_id'          => $request->farm_id,
        'supplier_id'      => $request->supplier_id,
        'invoice_no'       => $request->invoice_no,
        'invoice_date'     => $request->invoice_date,
        'parcel_no'        => $request->parcel_no,
        'parcel_through'   => $request->parcel_through,
        'total_parcel'     => $request->total_parcel,
        'amount'           => $request->amount,
        'sgst'             => $request->sgst,
        'cgst'             => $request->cgst,
        'igst'             => $request->igst,
        'total_amount'     => $request->total_amount
       ]);
       return redirect('invoice')->with('success','Invoice Updated Successfully!!!');
    }
    public function delete($id)
    {
       DB::table('invoices')
       ->where('invoice_id',$id)
       ->update([
        'is_active'     => 0
       ]);
       return redirect('invoice')->with('success','Invoice Deleted Successfully!!!');
    }
    
}
