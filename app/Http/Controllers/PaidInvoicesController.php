<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\PaidInvoices;

class PaidInvoicesController extends Controller
{
    public function index()
    {
       $PaidInvoiceAll = DB::table('paid_invoices')
       ->select('paid_invoices.*','firms.farm_name','suppliers.supplier_name')
       ->join('firms','paid_invoices.firm_id','=','firms.firm_id')
       ->join('suppliers','paid_invoices.supplier_id','=','suppliers.supplier_id')
       ->where('paid_invoices.is_active',1)
       ->get();
        return view('paid_invoices.paid_invoices',compact('PaidInvoiceAll'));
    }
    public function create()
    {
      $FarmAll = DB::table('firms')->select('firm_id','farm_name')->where('is_active',1)->get();
      $SupplierAll = DB::table('suppliers')->select('supplier_id','supplier_name')->where('is_active',1)->get();
        return view('paid_invoices.add',compact('FarmAll','SupplierAll'));
    }
    public function store(Request $request)
    {
       PaidInvoices::insert([
        'firm_id'          => $request->firm_id,
        'supplier_id'      => $request->supplier_id,
        'paid_amount'      => $request->paid_amount,
        'paid_date'        => $request->paid_date,
        'paid_by'          => $request->paid_by,
        'transaction_no'   => $request->transaction_no,
        'created_by'       => auth()->user()->id
       ]);
       return redirect('paid_invoices')->with('success','Supplier Added Successfully!!!');
    }
    public function edit($id)
    {
      $FarmAll = DB::table('firms')->select('firm_id','farm_name')->where('is_active',1)->get();
      $SupplierAll = DB::table('suppliers')->select('supplier_id','supplier_name')->where('is_active',1)->get();
      $PaidInvoiceAll = DB::select('select * from paid_invoices where paid_invoice_id="'.$id.'"');
      $paid_by_arr =array("CASH","RTGS","NEFT","IMPS","UPI");
      
      return view('paid_invoices.edit',compact('PaidInvoiceAll','FarmAll','SupplierAll','paid_by_arr'));
    }
    public function update(Request $request,$id)
    {
       DB::table('paid_invoices')
       ->where('paid_invoice_id',$id)
       ->update([
        'firm_id'          => $request->firm_id,
        'supplier_id'      => $request->supplier_id,
        'paid_amount'      => $request->paid_amount,
        'paid_date'        => $request->paid_date,
        'paid_by'          => $request->paid_by,
        'transaction_no'   => $request->transaction_no,
       ]);
       return redirect('paid_invoices')->with('success','Entry Updated Successfully!!!');
    }
    public function delete($id)
    {
       DB::table('paid_invoices')
       ->where('paid_invoice_id',$id)
       ->update([
        'is_active'     => 0
       ]);
       return redirect('paid_invoices')->with('success','Entry Deleted Successfully!!!');
    }
}
