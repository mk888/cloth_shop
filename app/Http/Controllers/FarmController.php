<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Firms;

class FarmController extends Controller
{
    public function index()
    {
       $FarmAll = DB::table('firms')
       ->select('*')
       ->where('is_active',1)
       ->get();
        return view('farm.farm',compact('FarmAll'));
    }
    public function create()
    {
        return view('farm.add');
    }
    public function store(Request $request)
    {
       Firms::insert([
        'farm_name'     => $request->farm_name,
        'farm_owner'    => $request->farm_owner,
        'gst_no'        => $request->gst_no,
        'pan_no'        => $request->pan_no,
        'address'       => $request->address,
        'created_by'    => auth()->user()->id
       ]);
       return redirect('firm')->with('success','Firm Added Successfully!!!');
    }
    public function edit($id)
    {
        $FirmData = DB::select('select * from firms where firm_id="'.$id.'"');
        return view('farm.edit',compact('FirmData'));
    }
    public function update(Request $request,$id)
    {
       DB::table('firms')
       ->where('firm_id',$id)
       ->update([
        'farm_name'     => $request->farm_name,
        'farm_owner'    => $request->farm_owner,
        'gst_no'        => $request->gst_no,
        'pan_no'        => $request->pan_no,
        'address'       => $request->address
       ]);
       return redirect('firm')->with('success','Firm Updated Successfully!!!');
    }
    public function delete($id)
    {
       DB::table('firms')
       ->where('firm_id',$id)
       ->update([
        'is_active'     => 0
       ]);
       return redirect('firm')->with('success','Firm Deleted Successfully!!!');
    }
    
}
